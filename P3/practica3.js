function cuadrados(){
	cuadradosText();
	var mensaje=prompt("Dame x:", "numero");
	var i=1;
	var respuesta="<br/><table><th>/Numero/</th><th>/Cuadrado/</th><th>/Cubo/</th>";
	for(i=1; i<= mensaje ; i++){
		respuesta+="<tr><td>"
		respuesta+=i;
		respuesta+="</td><td>";
		respuesta+=i*i;
		respuesta+="</td><td>";
		respuesta+=i*i*i;
		respuesta+="</td></tr>"
	}
	respuesta+="</table>"
	document.getElementById("respuesta").innerHTML=respuesta;
}

function suma(){
	sumaText();
	var numero1=Math.floor(Math.random()*1000)+1;
	var numero2=Math.floor(Math.random()*1000)+1;
	var suma=numero1+numero2;
	var hoy=new Date();
	var pregunta="Suma los siguientes números: "+numero1+"+"+numero2;
	var mensaje=prompt(pregunta, "");
	var hoy2=new Date();
	var tiempo=(hoy2.getTime()-hoy.getTime())/1000;
	if(numero1+numero2 != mensaje){
		var respuesta = "<br>Incorrecto. La respuesta correcta es: "+suma + ", y tardaste " + tiempo + " segundos en responder.";
	}
	else{
		var respuesta = "<br>Correcto. Tardaste " + tiempo + " segundos en responder.";
	}
	document.getElementById("respuesta").innerHTML=respuesta;

}



function crearArreglo(num){
	var i = 0;
	var numeros = new Array();
	for(i=0 ; i<num ; i++){
		numeros[i]=Math.floor(Math.random()*1000)-500;
	}
	return numeros;
}

function contador(){
	contadorText();
	var mensaje=prompt("Ingresa el valor de x:", "");
	arreglo = crearArreglo(mensaje);
	var respuesta="<br />Números: " + arreglo;
	var negativos = new Array();
	var cuentaNeg=0;
	var neutros = new Array();
	var cuentaCeros=0;
	var positivos = new Array();
	var cuentaPos=0;
	var i=0;
	for( i = 0 ; i<arreglo.length ; i++ ){
		if(arreglo[i]<0){
			negativos[cuentaNeg]=arreglo[i];
			cuentaNeg++;
		}
		else if(arreglo[i]==0){
			neutros[cuentaCeros]=arreglo[i];
			cuentaCeros++;
		}
		else{
			positivos[cuentaPos]=arreglo[i];
			cuentaPos++;
		}
	}
	respuesta+="<br />Negativos: " + cuentaNeg + " numeros: " + negativos;
	respuesta+="<br />Positivos: " + cuentaPos + " numeros: " + positivos;
	respuesta+="<br />Ceros: " + cuentaCeros;
	document.getElementById("respuesta").innerHTML=respuesta;
}



function sacaPromedio(renglon){
	var sum = 0;
	for(var i = 0; i < renglon.length; i++){
    	sum += parseInt(renglon[i]);
	}
	return sum/renglon.length;
}

function llenaMatriz(renglones, columnas){
	renglones = parseInt(renglones);
	columnas = parseInt(columnas);
	var matrix = new Array(renglones);
	for(var i=0 ; i<renglones ; i++){
		matrix[i] = new Array(columnas);
		for(var j=0; j<columnas ; j++){
			var numero=Math.floor(Math.random()*1000)+1;
			matrix[i][j]=numero;
		}
	}
	return matrix;

}

function crearMatriz(renglones, columnas){
	var x = new Array(renglones);
  	for (var i = 0; i < renglones; i++) {
  		y = new Array(columnas);
    	x[i] = y;
  	}
  	matriz = llenaMatriz(renglones,columnas);
	return matriz;
}

function promedioCompleto(renglones, columnas){
	matriz=llenaMatriz(renglones, columnas);
	var respuesta = "<br />Matriz: <br/>";
	for(var i=0 ; i<renglones ; i++){
		for(var j=0; j<columnas ; j++){
			respuesta+=matriz[i][j]+"&nbsp"
		}
		respuesta+="<br />";
	}
	respuesta += "<br />Promedios: <br/>";
	for(var i=0 ; i<renglones ; i++){
		respuesta+="Renglón #" +i+": " + sacaPromedio(matriz[i])+"<br/>";
	}
	return respuesta;


}


function promedios(){
	promediosText();
	var renglones=prompt("Ingresa el valor de x (renglones):", "numero");
	var columnas=prompt("Ingresa el valor de y (columnas)", "numero");

	var respuesta = promedioCompleto(renglones,columnas);
	document.getElementById("respuesta").innerHTML=respuesta;
}



function inverso(){
	var myVar;
	window.clearInterval(myVar);
	inversoText();
	var numero=Math.floor(Math.random()*10000000)+1;
	var x = numero;
	var y = 0;
	while (x>0) {
	    y *= 10;
	    y += x % 10;
	    x = Math.floor(x / 10)
	}
	document.getElementById("respuesta").innerHTML="<br/>Número aleatorio: "+numero + "<br/>Número invertido: "+y;
}



var precio = new Object();
	precio.cost = "";

function convertidor (num)
{
	if (num == "E")
	{
		pesos=document.getElementById("euro").value * 19.8;
		document.getElementById("peso").value=Math.round(pesos);
		precio.cost  = "El precio del euro es de 19.8 pesos";
	}
	else	
	{
		euros=document.getElementById("peso").value / 19.8;
		document.getElementById("euro").value=Math.round(euros);
        precio.cost  = "El precio del euro es de 19.8 pesos";
	}
}

function trigger ()
{
	alert(JSON.stringify(precio, null, 4));
}


function cuadradosText(){
	document.getElementById("pregunta").innerHTML="Para un número x, se devolverán todos los cuadrados y cubos desde 1 hasta x.";
	document.getElementById("respuesta").innerHTML="";
}

function sumaText(){
	document.getElementById("pregunta").innerHTML="Suma dos números aleatorios. Se devolverá retroalimentación y tiempo de respuesta.";
	document.getElementById("respuesta").innerHTML="";
}

function contadorText(){
	document.getElementById("pregunta").innerHTML="Se generan x números aleatorios, mismos que se clasificarán en negativos, positivos o ceros.";
	document.getElementById("respuesta").innerHTML="";
}

function promediosText(){
	document.getElementById("pregunta").innerHTML="Se obtendrá el promedio de cada renglón, de una matriz aleatoria de x renglones y y columnas.";
	document.getElementById("respuesta").innerHTML="";
}

function inversoText(){
	document.getElementById("pregunta").innerHTML="Se genera un número aleatorio entre 0 y 10000000 y se invertirán sus dígitos.";
	document.getElementById("respuesta").innerHTML="";
}



