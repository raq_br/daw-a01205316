//Java Script


function precio() {
    'use strict';
    var total, nconsulta = document.getElementById("a").value, nrayos = document.getElementById("b").value, nantibiotico = document.getElementById("c").value, ps = /[0-9]/, flag, flag2, flag3;
    flag = nconsulta.match(ps);
    flag2 = nrayos.match(ps);
    flag3 = nantibiotico.match(ps);
    
    if (!flag || !flag2 || !flag3 || nconsulta > 99 || nrayos > 99 || nantibiotico > 99) {
        window.alert("Inserte una cantidad valida (max. 99) ");
    } else {
        total = (nconsulta * 600 + nrayos * 1000 + nantibiotico * 350) * 1.16;
        document.getElementById("total").value = total;
    }
    
}


function mOver(obj) {
    'use strict';
    obj.innerHTML = "Consulta, rayosX o antibioticos";
}

function mOut(obj) {
    'use strict';
    obj.innerHTML = "";
}

function allowDrop(ev) {
    'use strict';
    ev.preventDefault();
}

function drag(ev) {
    'use strict';
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    'use strict';
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
