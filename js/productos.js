$( document ).ready(function() {
    $("#computacion").collapse('show');
});

$('#computacion').on('show.bs.collapse', function () {
    $("#almacenamiento").collapse('hide');
    $("#escolar").collapse('hide');
    $("#papeleria").collapse('hide');
});
$('#almacenamiento').on('show.bs.collapse', function () {
    $("#computacion").collapse('hide');
    $("#escolar").collapse('hide');
    $("#papeleria").collapse('hide');
});
$('#escolar').on('show.bs.collapse', function () {
    $("#almacenamiento").collapse('hide');
    $("#computacion").collapse('hide');
    $("#papeleria").collapse('hide');
});
$('#papeleria').on('show.bs.collapse', function () {
    $("#almacenamiento").collapse('hide');
    $("#escolar").collapse('hide');
    $("#computacion").collapse('hide');
});

function irContacto(){
    window.location.assign("index.html")
}