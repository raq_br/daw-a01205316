//JavaScript

function genTable() {
    'use strict';
    
    var lnam = document.getElementById("lastname").value,
        nam = document.getElementById("name").value,
        bday = document.getElementById("cumpleaños").value,
        gender = document.getElementsByName("sex"),
        telf = document.getElementById("tel").value,
        email = document.getElementById("mail").value,
        ps = /^[A-Za-z]\w{2,20}$/,
        emal = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\w{4,30}$/,
        generate = true,
        //flag = pass.localeCompare(val),
        i,
        j,
        k,
        campos = ["Lastname", "Name", "Cumpleaños", "Gender", "Phone", "E-mail"],
        info = [lnam, nam, bday, gender, telf, email],
        cell,
        myTableDiv = document.getElementById("Table"),
        tab = document.createElement('TABLE'),
        tableBody = document.createElement('TBODY'),
        tr,
        td;
    
    if (lnam.match(ps) && nam.match(ps)) {
        generate = true;
    } else {
        window.alert("Error, verifique el campo de nombre y apellido.");
        generate = false;
    }
    
    if (!bday) {
        window.alert("Error, verifique el campo de cumpleaños.");
    }
    
    if (email.match(emal)) {
        window.alert("Error, verifique el campo de correo electronico");
    }
    
    if (/\D/.test(telf)) {
        window.alert("Error, verifique el campo de telefono.");
    }
    
    if (!bday && emal.test(email) && /\D/.test(telf)) {
        window.alert("Error, llene todos los campos.");
        generate = false;
    }

    tab.border = '1';
    
    
    tab.appendChild(tableBody);
    
    if (generate) {
        for (i = 0; i < 6; i += 1) {
            tr = document.createElement('TR');
            tableBody.appendChild(tr);
       
            for (j = 0; j < 2; j += 1) {
                td = document.createElement('TD');
                td.width = '75';
                if (j == 0) {
                    td.appendChild(document.createTextNode(campos[i]));
                    tr.appendChild(td);
                } else {
                    if (i == 3) {
                        if (gender[0].checked) {
                            td.appendChild(document.createTextNode(gender[0].value));
                        } else if (gender[1].checked) {
                            td.appendChild(document.createTextNode(gender[1].value));
                        }
                    } else {
                        td.appendChild(document.createTextNode(info[i]));
                    }
                    tr.appendChild(td);
                }
            }
        }
        myTableDiv.appendChild(tab);
    }    
    
}


