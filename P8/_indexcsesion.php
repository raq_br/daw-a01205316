<!DOCTYPE html>
<?php session_start(); ?>

<head>
    <link rel="stylesheet" type= "text/css" href="css/estilo.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
	<br>
	<br>
<table cellspacing="50">
	<tr>
		<td>
        		<a href="#" class="myButton">Clases</a><br><br>
       			<a href="#" class="myButton">Contacto</a><br><br>
        		<a href="#" class="myButton">Profesores</a><br><br>
        		<a href="#" class="myButton">Inscribir</a><br><br>
        	</td>
        	<td>
        		<?php
        			echo "Bienvenido ".$_POST["user"]." que tengas un buen día";
        		?>
        		<p>Por favor sube la imagen que será guardada como tu foto de alumno:</p>
        		<form method="post" enctype="multipart/form-data" action="_upload.php" class="input-list style-2 clearfix">
            			<input type="file" name="fotoperfil" id="fotoperfil" class="myButton"><br>
            			<input type="submit" value="Subir" class="myButton">
        		</form>
        	</td>
     	</tr>
     	
</table>

<h2>¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</h2>
<p>Elimina todas las variables de sesión que hay es importante hacerlo antes de destruir la sesión para liberar el espacio en memoria y no acumular lo de todas las sesiones. </p>
<h2>¿Cuál es la diferencia entre una variable de sesión y una cookie?</h2>
<p>La variable de sesión es por tiempo corto y está establecida en el servidor. La cookie es un archivo con la información de la sesión pero se guarda en la computadora, por lo que se puede guardar más tiempo pero solo se puede acceder desde la computadora donde se generó.</p>
<h2>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?</h2>
<p>Existe la función file_exists() que nos permite revisar si ese archivo ya está en el servidor y de esta manera podemos evitar que se repita.</p>

<br>
<br>