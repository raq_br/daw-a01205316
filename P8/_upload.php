<?php
$target_dir = "assets/";
$target_file = $target_dir . basename($_FILES["fotoperfil"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fotoperfil"]["tmp_name"]);
    if($check !== false) {
        echo "El archivo es una - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "Error! El archivo que intentas subir no es una imagen válida..";
        $uploadOk = 0;
    }
}

// Check file size
if ($_FILES["fotoperfil"]["size"] > 500000) {
    echo "Error! El archivo que intentas subir es muy grande.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Error! Solo se pueden subir archivos JPG, JPEG, PNG & GIF.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "El archivo no se pudo subir. Intenta más tarde.";
    sleep(10);
    header("Location: index.php");
    die();
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fotoperfil"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fotoperfil"]["name"]). " ha sido subido exitosamente.";
        sleep(10);
    	header("Location: index.php");
    die();
    } else {
        echo "Error! Hubo un problema con la subida del archivo. Intenta más tarde.";
        sleep(10);
    	header("Location: index.php");
    	die();
    }
}
?>